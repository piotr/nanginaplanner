#!/usr/local/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.shortcuts import render
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from planner.models import Weekend, PlannerUser
import datetime
from django.core.mail import send_mail, EmailMessage
from smtplib import SMTPException
from django.db.models import Q

fromaddress = "Nangina-Planer <nangina@klabusnik.de>"

def newuser_admin(user):
    admins = PlannerUser.objects.filter(is_admin=True)

    for admin in admins:
        context = {"admin" : admin, "new" : user}
        text = loader.render_to_string("planner/mails/please_verify_mail.txt", context)
        admin.email_user(user.first_name + " " + user.last_name + " hat sich registriert", text)

def leader_signup(leader, weekend):
    admins = PlannerUser.objects.filter(is_admin=True)

    for admin in admins:
        context = {"admin" : admin, "leader" : leader, "weekend" : weekend}
        text = loader.render_to_string("planner/mails/leader_signup_mail.txt", context)
        admin.email_user("Leitung Wochenende in " + weekend.location, text)

def newuser_welcome(user):
    context = {"user" : user}
    text = loader.render_to_string("planner/mails/newuser_welcome_mail.txt", context)
    user.email_user("Wilkommen beim Nangina-Planer", text)

def newuser_verified(user):
    context = {"user" : user}
    text = loader.render_to_string("planner/mails/newuser_verified_mail.txt", context)
    user.email_user("Freischaltung beim Nangina-Planer", text)

def user_added(user, weekend):
    context = {"user": user, "w" : weekend}
    text = loader.render_to_string("planner/mails/user_added_mail.txt", context)
    user.email_user("Nangina-Wochenende in " + weekend.location, text)

def user_comment(comment):
    readers = comment.weekend.participants.filter(~Q(pk=comment.user.pk))
    for reader in readers:
        context = {"comment": comment, "user": reader}
        text = loader.render_to_string("planner/mails/user_comment_mail.txt", context)
        reader.email_user("Kommentar zum Wochenende in " + comment.weekend.location, text)

def user_signup(user, weekend):
    context = {"user": user, "weekend": weekend}
    text = loader.render_to_string("planner/mails/user_signup_mail.txt", context)
    weekend.leader.email_user("Anmeldung zum Wochenende in " + weekend.location, text)
