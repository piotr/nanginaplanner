Hallo {{weekend.leader.first_name}},

{{user.first_name}} {{user.last_name}} hat sich zum Wochenende in {{weekend.location}} angemeldet.

Viele Grüße
Der Nanginaplanungsroboter

-------

PS: Diese Mail wurde automatisch abgeschickt. Bitte nicht drauf antworten.

https://nangina.klabusnik.de