from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseNotFound
from planner.models import Weekend, PlannerUser, Comment
from django.contrib.auth import authenticate, login, logout
from django.template.defaultfilters import last
from mailer import mailer

import views


def planneruser(user):
    if not (user.username == ""):
        return PlannerUser.objects.get(username=user.username)
    else:
        return None

def delete_participant(request, w_id, p_id):
    weekend = Weekend.objects.get(id=w_id)
    user = planneruser(request.user)
    deleted_part = PlannerUser.objects.get(username=p_id)
    if user.is_authenticated() and (user.is_admin or ((weekend.leader.pk == user.pk) if weekend.leader != None else False)):
        weekend.remove_participant(deleted_part)
        weekend.save()
        return HttpResponseRedirect(reverse('weekend', args=(w_id,)))
    else:
        return HttpResponseNotFound('<h1>Versucht da jemand einfach Teilnehmer zu l&ouml;schen? Ich glaubs euch wohl!</h1>')


def register(request, w_id):
    weekend = Weekend.objects.get(id=w_id)
    user = planneruser(request.user)

    if user.is_authenticated():
        if weekend.participants.count() >= 9:
            return views.weekend(request, w_id, {'full': True})
        weekend.participants.add(user)
        weekend.save()
        user.save()
        if weekend.leader != None:
            mailer.user_signup(user, weekend)
        return HttpResponseRedirect(reverse('weekend', args=(w_id,)))

def add_participant(request, w_id):
    weekend = Weekend.objects.get(id=w_id)
    user = planneruser(request.user)
    if user.is_authenticated and (user.is_admin or ((weekend.leader.pk == user.pk) if weekend.leader != None else False)):
        selected_name = request.POST['userselect']
        selected_user = PlannerUser.objects.get(username=selected_name)
        weekend.participants.add(selected_user)
        weekend.save()
        mailer.user_added(selected_user, weekend)
        return HttpResponseRedirect(reverse('weekend', args=(w_id,)))


def register_leader(request, w_id):
    weekend = Weekend.objects.get(id=w_id)
    user = planneruser(request.user)
    weekend.set_leader(user)
    weekend.save()
    user.save()
    mailer.leader_signup(user, weekend)
    return HttpResponseRedirect(reverse('weekend', args=(w_id,)))

def unregister_leader(request, w_id):
    weekend = Weekend.objects.get(id=w_id)
    user = planneruser(request.user)
    weekend.unset_leader(user)
    weekend.save()
    user.save()
    return HttpResponseRedirect(reverse('weekend', args=(w_id,)))



def submitreg(request):
    name = request.POST['username']
    pw = request.POST['pw']
    pwconf = request.POST['pwconf']
    first = request.POST['firstname']
    last = request.POST['lastname']
    email = request.POST['email']
    phone = request.POST['phone']
    role1 = True if 'role1' in request.POST else False
    role2 = True if 'role2' in request.POST else False
    role3 = True if 'role3' in request.POST else False
    speech = True if 'speech' in request.POST else False


    pwerr = not pw == pwconf
    unameerr = PlannerUser.objects.filter(username=name).exists()

    if pwerr or unameerr:
        return render(request, 'planner/reguser.html', {"pwerr": pwerr, "unameerr": unameerr})

    user = PlannerUser.objects.create_user(username=name, password=pw)
    user.first_name = first
    user.last_name = last
    user.email = email
    user.telephone = phone
    user.role1 = role1
    user.role2 = role2
    user.role3 = role3
    user.speech = speech

    user.is_active = False

    user.save()
    mailer.newuser_admin(user)
    mailer.newuser_welcome(user)

    return render(request, 'planner/weekends.html')

def edituser(request):
    context = {'loguser' : planneruser(request.user)}
    return render(request, 'planner/edituser.html', context)

def submitedit(request):
    if request.user.is_authenticated():
        first = request.POST['firstname']
        last = request.POST['lastname']
        email = request.POST['email']
        phone = request.POST['phone']
        role1 = True if 'role1' in request.POST else False
        role2 = True if 'role2' in request.POST else False
        role3 = True if 'role3' in request.POST else False
        speech = True if 'speech' in request.POST else False

        user = planneruser(request.user)
        user.first_name = first
        user.last_name = last
        user.email = email
        user.telephone = phone
        user.role1 = role1
        user.role2 = role2
        user.role3 = role3
        user.speech = speech

        user.save()

        context = {'loguser' : planneruser(request.user), 'success': True}
        return render(request, 'planner/edituser.html', context)
    else:
        return HttpResponseNotFound('<h1>Nicht angemeldet</h1>')

def promote(request, u_id):
    if request.user.is_authenticated() and planneruser(request.user).is_admin:
        user = PlannerUser.objects.get(username=u_id)
        user.is_leader = True
        user.save()
    return HttpResponseRedirect(reverse('user', args=(u_id,)))

def demote(request, u_id):
    if request.user.is_authenticated() and planneruser(request.user).is_admin:
        user = PlannerUser.objects.get(username=u_id)
        user.is_leader = False
        for w in user.leading.all():
            w.leader = None
            w.save()
        user.save()
    return HttpResponseRedirect(reverse('user', args=(u_id,)))

def submitweekend(request):
    if request.user.is_authenticated() and planneruser(request.user).is_admin:
        location = request.POST['location']
        description = request.POST['description']
        startdate = request.POST['startdate']
        enddate = request.POST['enddate']
        w = Weekend.objects.create(location=location, description=description, startdate=startdate, enddate=enddate)
        w.save()
    return HttpResponseRedirect(reverse('weekends', args=()))

def editweekend(request, w_id):
    if request.user.is_authenticated() and planneruser(request.user).is_admin:
        location = request.POST['location']
        description = request.POST['description']
        startdate = request.POST['startdate']
        enddate = request.POST['enddate']
        w = Weekend.objects.get(id = w_id)
        w.location = location
        w.description = description
        w.startdate = startdate
        w.enddate = enddate
        w.save()
    return HttpResponseRedirect(reverse('weekend', args=(w_id,)))

def editdesc(request, w_id):
    if request.user.is_authenticated():
        w = Weekend.objects.get(id=w_id)
        if planneruser(request.user) == w.leader:
            description = request.POST['description']
            w.description = description
            w.save()
    return HttpResponseRedirect(reverse('weekend', args=(w_id,)))

def editpassword(request):
    if request.user.is_authenticated():
        oldpass = request.POST['opw']
        npw = request.POST['npw1']
        npwconf = request.POST['npw2']

        u = authenticate(username=request.user.username, password=oldpass)

        opwerr = False
        if u is None:
            opwerr = True

        npwerr = not npw == npwconf

        if opwerr or npwerr:
            context = {'loguser' : planneruser(request.user), 'opwerr': opwerr, 'npwerr': npwerr}
            return render(request, 'planner/edituser.html', context)

        request.user.set_password(npw)
        request.user.save()
        context = {'loguser' : planneruser(request.user), 'opwerr': opwerr, 'npwerr': npwerr, 'pwsuccess': True}

        return render(request, 'planner/edituser.html', context)

def deleteweekend(request, w_id):
    if request.user.is_authenticated() and planneruser(request.user).is_admin:
        Weekend.objects.get(id=w_id).delete()
    return HttpResponseRedirect(reverse('weekends', args=()))


def login_con(request):
    name = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(username=name, password=password)

    if user is not None:
        if user.is_active:
            login(request, user)
        else:
            return render(request, 'planner/weekends.html', {'notverified': True, 'weekends': Weekend.objects.extra(order_by=['startdate'])})
    else:
        return render(request, 'planner/weekends.html', {'loginerr': True, 'weekends': Weekend.objects.extra(order_by=['startdate'])})


    return HttpResponseRedirect(reverse('weekends', args=()))


def logout_con(request):
    logout(request)
    return HttpResponseRedirect(reverse('weekends', args=()))

def submitcomment(request, w_id):
    if request.user.is_authenticated():
        user = planneruser(request.user)
        text = request.POST.get("commenttext", False)
        weekend = Weekend.objects.get(id=w_id)
        comment = Comment.objects.create(text=text, user=user, weekend = weekend)
        comment.save()
        mailer.user_comment(comment)
        return HttpResponseRedirect(reverse('weekend', args=(w_id,)))

def deletecomment(request, w_id, c_id):
    if request.user.is_authenticated():
        comment = Comment.objects.get(id=c_id)
        user = planneruser(request.user)
        weekend = Weekend.objects.get(id=w_id)
        if comment.user == user or user.is_admin:
            comment.delete()
        return HttpResponseRedirect(reverse('weekend', args=(w_id,)))

def verifyuser(request, u_id):
    loguser = planneruser(request.user)
    if loguser.is_authenticated() and loguser.is_admin:
        user = PlannerUser.objects.get(username=u_id)
        user.is_active = True
        user.save()
        mailer.newuser_verified(user)
        return HttpResponseRedirect(reverse('users', args=()))

def denyuser(request, u_id):
    loguser = planneruser(request.user)
    if loguser.is_authenticated() and loguser.is_admin:
        user = PlannerUser.objects.get(username=u_id)
        user.delete()
        return HttpResponseRedirect(reverse('users', args=()))
