"""Weekend Planner Models"""
from django.db import models
from django.contrib.auth.models import User, UserManager
#from django.db.models.fields.related import ForeignKey

# Create your models here.

class PlannerUser(User):
    """User for weekend planner"""
    is_leader = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    telephone = models.CharField(max_length=20)
    role1 = models.BooleanField(default=False)
    role2 = models.BooleanField(default=False)
    role3 = models.BooleanField(default=False)
    speech = models.BooleanField(default=False)
    objects = UserManager()

    def __str__(self):
        return self.first_name + " " + self.last_name

class Weekend(models.Model):
    """Model for Weekend"""
    startdate = models.DateField(auto_now=False)
    enddate = models.DateField(auto_now=False)
    location = models.CharField(max_length=100)

    leader = models.ForeignKey(PlannerUser, related_name='leading', null=True, blank=True)
    participants = models.ManyToManyField(PlannerUser, related_name='subscriptions')

    description = models.TextField(null=True, blank=True)

    def set_leader(self, u):
        self.leader = u
        self.participants.add(u)

    def unset_leader(self, u):
        self.leader = None
        self.participants.remove(u)

    def remove_participant(self, u):
        if not self.leader is None:
            if u.pk == self.leader.pk:
                self.leader = None
        self.participants.remove(u)

    def __str__(self):
        return self.location + ", " + str(self.startdate)

class Comment(models.Model):
    """Comment for weekends"""
    text = models.CharField(max_length=2000)
    user = models.ForeignKey(PlannerUser, null=True, blank=False)
    date = models.DateTimeField(auto_now=True)
    weekend = models.ForeignKey(Weekend, related_name='comments')
