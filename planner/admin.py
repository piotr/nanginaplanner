"""Admin settings for planner models"""
from django.contrib import admin
from planner.models import Weekend, PlannerUser, Comment
# Register your models here.

class WeekendAdmin(admin.ModelAdmin):
    """Admin settings for weekend"""
    fields = ['startdate', 'enddate', 'location', 'leader', 'participants']

class CommentAdmin(admin.ModelAdmin):
    """Admin settings for comment"""
    fields = ['date', 'text', 'user', 'weekend']

admin.site.register(Weekend, WeekendAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(PlannerUser)
