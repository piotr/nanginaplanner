"""View functions"""
import datetime
from django.shortcuts import render
from django.http import HttpResponse
from planner.models import Weekend, PlannerUser
from django.core import serializers
from django.http import JsonResponse
import json

# Create your views here.

def jadetest(request):
    return render(request, "planner/jadetest.jade")

def planneruser(user):
    if not (user.username == ""):
        return PlannerUser.objects.get(username=user.username)
    else:
        return None

def index(request):
    return HttpResponse("Hello, world.")

def weekends(request):
    oldweekends = Weekend.objects.filter(enddate__lt=datetime.date.today()).order_by('-startdate')
    currentweekends = Weekend.objects.filter(enddate__gte=datetime.date.today()).order_by('startdate')
    #context = {'weekends' : weekends, 'loguser' : planneruser(request.user)}
    context = {
        'oldweekends' : oldweekends,
        'currentweekends' : currentweekends,
        'loguser' : planneruser(request.user)
        }
    return render(request, 'planner/weekends.html', context)

def weekend(request, w_id, errors={}):
    weekend = Weekend.objects.get(id=w_id)
    users = PlannerUser.objects.all()
    context = {
        'weekend' : weekend,
        'loguser' : planneruser(request.user),
        'errors': errors,
        'users': users
        }
    return render(request, 'planner/weekend.html', context)

def user(request, username):
    user = PlannerUser.objects.get(username=username)
    participations = (user.subscriptions.all())
    context = {
        'user' : user,
        'participations' : participations,
        'loguser' : planneruser(request.user)
        }
    return render(request, 'planner/user.html', context)

def users(request):
    """Displays users"""
    users = PlannerUser.objects.filter(is_active=True).order_by("last_name")
    newusers = PlannerUser.objects.filter(is_active=False).order_by("last_name")
    context = {
        'users' : users,
        'newusers' : newusers,
        'loguser' : planneruser(request.user)
        }
    return render(request, 'planner/users.html', context)

def register_user(request):
    return render(request, 'planner/reguser.html')

def documents(request):
    """Displays downloadable documents to registered users"""
    context = {'loguser': planneruser(request.user)}
    return render(request, 'planner/documents.html', context)

def restweekends(request):
    weekends = Weekend.objects.filter(enddate__gte=datetime.date.today()).order_by('startdate')
    data = serializers.serialize('json', weekends, fields=('location','startdate', 'enddate', 'description'))
    jsondict = json.loads(data)
    response = JsonResponse({"weekends": jsondict})
    response['Access-Control-Allow-Origin'] = "*"
    return response
    
