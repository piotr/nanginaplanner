"""Email notification command for cron"""
import datetime
from django.core.management.base import BaseCommand
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist

from planner.models import Weekend, PlannerUser


def planneruser(user):
    """Turns user into planneruser"""
    if not user.username == "":
        return PlannerUser.objects.get(username=user.username)
    else:
        return None

class Command(BaseCommand):
    """Sends out notification E-Mails"""
    help = 'Sends out notification E-Mails'

    def handle(self, *args, **options):

        today = datetime.date.today()

        try:
            weekend = Weekend.objects.get(
                startdate__lte=today+datetime.timedelta(7),
                startdate__gte=today
                )

            for participant in weekend.participants.all():
                if participant.email and participant.email != "":
                    context = {"p" : planneruser(participant), "w" : weekend}
                    text = loader.render_to_string("planner/mails/notification_mail.txt", context)
                    participant.email_user("Nangina-Wochenende in " + weekend.location, text)


        except ObjectDoesNotExist:
            print("No Weekend Found...")
