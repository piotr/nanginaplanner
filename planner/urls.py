from django.conf.urls import url

from planner import views
from planner import controllers

urlpatterns = [
    url(r'^$', views.weekends, name='weekends'),
    url(r'^weekends/$', views.weekends, name='weekends'),
    url(r'^weekend/(?P<w_id>\d+)/$', views.weekend, name='weekend'),
    url(r'^users/$', views.users, name='users'),
    url(r'^user/(?P<username>[-\.\w]+)/$', views.user, name='user'),
    url(r'^reguser/$', views.register_user, name="reguser"),
    url(r'^documents/$', views.documents, name="documents"),
    url(r'^restweekends$', views.restweekends, name="restweekends"),


    url(r'^login/$', controllers.login_con, name="login"),
    url(r'^logout/$', controllers.logout_con, name="logout"),
    url(r'^register/(?P<w_id>\d+)/$', controllers.register, name="register"),
    url(r'^addparticipant/(?P<w_id>\d+)/$', controllers.add_participant, name="addparticipant"),
    url(r'^delp/(?P<w_id>\d+)\_(?P<p_id>[-\.\w]+)/$', controllers.delete_participant, name="delp"),
    url(r'^submitreg/$', controllers.submitreg, name='submitreg'),
    url(r'^promote/(?P<u_id>[-\.\w]+)/$', controllers.promote, name="promote"),
    url(r'^demote/(?P<u_id>[-\.\w]+)/$', controllers.demote, name="demote"),
    url(r'^registerleader/(?P<w_id>\d+)/$', controllers.register_leader, name="registerleader"),
    url(r'^unregister/(?P<w_id>\d+)/$', controllers.unregister_leader, name="unregisterleader"),
    url(r'^submitweekend/$', controllers.submitweekend, name='submitweekend'),
    url(r'^edituser/$', controllers.edituser, name='edituser'),
    url(r'^submitedit/$', controllers.submitedit, name='submitedit'),
    url(r'^delw/(?P<w_id>\w+)/$', controllers.deleteweekend, name='delw'),
    url(r'^editweekend/(?P<w_id>\w+)$', controllers.editweekend, name='editweekend'),
    url(r'^editdesc/(?P<w_id>\w+)$', controllers.editdesc, name='editdesc'),
    url(r'^editpassword/$', controllers.editpassword, name='editpassword'),
    url(r'^submitcomment/(?P<w_id>\w+)$', controllers.submitcomment, name='submitcomment'),
    url(r'^submitcomment/(?P<w_id>[-\.\w]+)/(?P<c_id>\w+)$', controllers.deletecomment, name='deletecomment'),
    url(r'^verifyuser/(?P<u_id>[-\.\w]+)$', controllers.verifyuser, name="verifyuser"),
    url(r'^denyuser/(?P<u_id>[-\.\w]+)$', controllers.denyuser, name="denyuser"),

    

]
