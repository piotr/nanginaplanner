var main = function() {
	setTimeout(function(){
	  $(".fadeout").fadeOut(2000)}, 5000);

	$('.newwe').click(function(){
		$('#wecreator').slideDown();
	});

	$('.cancel').click(function(){
		$('#wecreator').slideUp();
		$('#descedit').slideUp();
	});

	$('.descedit').click(function(){
		$('#descedit').slideDown();
	});

	$('.delw').click(function(){
		var really = $(this).parent().siblings('.really')
		really.slideDown()
		setTimeout(function(){
			really.slideUp(1000)
		}, 5000)
	})

	$('.no').click(function(){
		$(this).parent().parent().slideUp()
	})

	$('.adduser').click(function(){
		$('#useradder').slideDown()
	})

	$('.canceladd').click(function(){
		$('#useradder').slideUp()
	})

	$('.expandold').click(function(){
		if (!$('#oldweekends').is(":visible")) {
		  $('#oldweekends').slideDown()
		  $(this).html('<span class="glyphicon glyphicon-triangle-top"></span> Alte Wochenenden ausblenden')
		} else {
		  $('#oldweekends').slideUp()
		  $(this).html('<span class="glyphicon glyphicon-triangle-bottom"></span> Alte Wochenenden anzeigen')
		}
	})



	$('#location, #startdate, #enddate').bind("input change", checkinput_weekend)

	$('#username, #firstname, #lastname, #email, #phone, #pw, #pwconf').bind("input change", checkinput_user)

	$('#opw, #npw1, #npw2').bind("input change", checkinput_pw)

}

checkinput_pw = function(){
	if($('#opw').val() == "" || $('#npw1').val() == "" || $('#npw2').val == ""){
		$('#submitpw').prop('disabled', true)
	} else {
		$('#submitpw').prop('disabled', false)
	}
}

checkinput_user = function(){
	if ($('#username').val() == "" || $('#username').val().indexOf(" ") > -1  || $('#username').val().indexOf("ä") > -1 ||
			$('#username').val().indexOf("ö") > -1 || $('#username').val().indexOf("ü") > -1 || $('#username').val().indexOf("ß") > -1 ||
			$('#firstname').val() == "" ||
			$('#lastname').val() == "" || $('#email').val() == "" ||  $('#phone').val() == "" || $('#pw').val() == "" || $('#pwconf').val() == ""){
		$('#submituser').prop('disabled', true)
	} else {
		$('#submituser').prop('disabled', false)
	}
}

checkinput_weekend = function(){
	if ($('#startdate').val() == "" || $('#enddate').val() == "" || $('#location').val() == ""){
		$('#submitweekend').prop('disabled', true)
	} else {
		$('#submitweekend').prop('disabled', false)
	}
};

$(function() {
	$( ".datepicker" ).datepicker({dateFormat: "yy-mm-dd", onSelect: function(dateText){
		if($(this).attr("id") == "startdate" && $("#enddate").val() == "")  {
			var enddate = new Date(dateText)
			enddate.setDate(enddate.getDate() + 1)
			$("#enddate").val(enddate.toISOString().slice(0,10))
			checkinput_weekend()
		}
		if($(this).attr("id") == "enddate" || $(this).attr("id") == "startdate") {
			checkinput_weekend()
		}
	}});
});

$(document).ready(main)
