from planner.models import Weekend, PlannerUser

def save(obj):
    """Saves database"""
    for x in obj:
        x.save()

def populate():
    """Populates database with some initial test data"""
    u1 = PlannerUser.objects.create_user(username="ribbi", password="bla")
    u2 = PlannerUser.objects.create_user(username="cstritzke", password="bla")
    u3 = PlannerUser.objects.create_user(username="kathi", password="bla")
    u4 = PlannerUser.objects.create_user(username="batman", password="bla")


    u1.first_name = "Friedhelm"
    u1.last_name = "Ribberger"
    u1.is_leader = True
    u1.is_admin = True
    u1.telephone = "1489210"
    u1.save()

    u2.first_name = "Christian"
    u2.last_name = "Stritzke"
    u2.is_leader = True
    u2.is_admin = True
    u2.telephone = "1489210"
    u2.save()

    u3.first_name = "Katharina"
    u3.last_name = "Stritzke"
    u3.is_leader = True
    u3.is_admin = True
    u3.telephone = "1489210"
    u3.save()

    u4.first_name = "Bruce"
    u4.last_name = "Wayne"
    u4.is_leader = False
    u4.is_admin = False
    u4.telephone = "1489210"
    u4.save()


    save({u1, u2, u3, u4})

    w1 = Weekend(location="Finnentrop", date="2014-03-07")
    w1.save()
    w2 = Weekend(location="Dortmund", date="2014-06-24")
    w2.save()
    w3 = Weekend(location="Osnabrück", date="2014-10-19")
    w3.save()

    save({w1, w2, w3})

    w1.set_leader(u1)
    w2.set_leader(u2)
    w3.set_leader(u3)

    save({w1,w2,w3})

    w1.participants.add(u4)
    w1.save()

    w3.participants.add(u1)
    w3.save()
    w3.participants.add(u2)
    w3.save()

    w2.participants.add(u4)
    w2.participants.add(u3)
    w2.participants.add(u1)

    save({w1,w2,w3})
