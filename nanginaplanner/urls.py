from django.conf.urls import include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'nanginaplanner.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^admin/', include(admin.site.urls)),
	#url(r'^password/', include('django.contrib.auth.urls')),
	url(r'^$', include('planner.urls')),
    url(r'^planner/', include('planner.urls')),
]
